import { Injectable } from '@angular/core';
import {HttpClient,HttpRequest,HttpEventType,HttpResponse} from '@angular/common/http';
import {Observable,Subject} from 'rxjs';
@Injectable()
export class UploadService {

  constructor(private http:HttpClient) {

  }

  public upload(url,file:File):Observable<number>{
    const status = {};
    const formData: FormData = new FormData();
    formData.append('file',file,file.name);
    const req = new HttpRequest('POST',url,formData,{
      reportProgress : true
    })
    const progress = new Subject<number>()
    this.http.request(req).subscribe(event=>{
      if(event.type===HttpEventType.UploadProgress) {
        const percentDone = Math.round(100 * event.loaded / event.total);
        progress.next(percentDone)
      }else if (event instanceof HttpResponse) {
        progress.complete()
      }
    })

    return progress.asObservable()
  }

}
