import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadComponent } from './upload.component';
import { MatButtonModule, MatDialogModule, MatListModule, MatProgressBarModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import {UploadService} from './upload.service';

@NgModule({
  imports: [CommonModule, MatButtonModule, MatDialogModule, MatListModule, MatProgressBarModule],
  declarations: [UploadComponent],
  exports: [UploadComponent],
  providers: [UploadService]

})
export class UploadModule {}
