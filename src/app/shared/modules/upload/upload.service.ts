import { Injectable } from '@angular/core';
import {HttpClient,HttpRequest,HttpEventType,HttpResponse} from '@angular/common/http';
import {Observable,Subject} from 'rxjs';
import {
  AngularFireStorage,
  AngularFireUploadTask
} from '@angular/fire/storage';
import {tap,finalize} from 'rxjs/operators'
@Injectable()
export class UploadService {

  task: AngularFireUploadTask;

// Progress monitoring
  percentage: Observable<number>;

  snapshot: Observable<any>;

  // Download URL
  downloadURL: Observable<string>;

  // State for dropzone CSS toggling
  isHovering: boolean;

  constructor(private http:HttpClient,private storage:AngularFireStorage) {

  }

  public upload(url,file:File,path:string):Observable<any>{
    /*const status = {};
    const formData: FormData = new FormData();
    formData.append('file',file,file.name);
    const req = new HttpRequest('POST',url,formData,{
      reportProgress : true
    })
    const progress = new Subject<number>()
    this.http.request(req).subscribe(event=>{
      if(event.type===HttpEventType.UploadProgress) {
        const percentDone = Math.round(100 * event.loaded / event.total);
        progress.next(percentDone)
      }else if (event instanceof HttpResponse) {
        progress.complete()
      }
    })

    return progress.asObservable() */




    this.task = this.storage.upload(path, file);
    return this.task.percentageChanges()

  }

}
