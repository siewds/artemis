import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {UploadService} from './upload.service';
import {Observable,Subject} from 'rxjs';
import {
  AngularFireStorage,
  AngularFireUploadTask
} from '@angular/fire/storage';
import {tap,finalize} from 'rxjs/operators'

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  @Input() url: string;
  uploading = false;
  selectedFile: File;
  value: number = 0;
  task: AngularFireUploadTask;
  percentage: Observable<number>;
  uploadSuccessful:Subject<string>;
  @Output() isFileValid: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(private service: UploadService,private storage:AngularFireStorage) { }

  ngOnInit() {
    this.value = 0;
  }

  selectFile(event) {
    [this.selectedFile] = event.target.files;
    if(this.selectedFile && !this.fileIsZip(this.selectedFile) ) {
      this.selectedFile = null;

    }
    this.isFileValid.emit(this.selectedFile ? true : false);
  }

  fileIsZip(file) {
    console.log(file)
    var extension = file.name.split('.').pop();
    return extension == 'zip'
  }

  isValid() {
    if(this.selectedFile) {
      return true;
    }else {
      return false;
    }
  }


  storageUpload(path,file) {
    this.task = this.storage.upload(path,file);
    this.percentage = this.task.percentageChanges().pipe(
      finalize(()=>{
        console
        this.uploading = false;
        this.percentage = null;
        this.storage.ref(path).getDownloadURL().subscribe((val)=>{
          this.uploadSuccessful.next(val);
          this.uploadSuccessful.complete()
        })
      })
    )
    return this.task.snapshotChanges()
  }

  upload() {
    this.uploading = true;
    this.uploadSuccessful = new Subject<string>()
    const path = `${new Date().getTime()}_${this.selectedFile.name}`;
    this.storageUpload(path,this.selectedFile).subscribe()
    return this.uploadSuccessful.asObservable()
  }

}
