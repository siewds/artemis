import {NgModule} from '@angular/core';

import {
  MatProgressSpinnerModule,
  MatCardModule,
  MatCheckboxModule,
} from '@angular/material';

@NgModule({
  imports: [
    MatProgressSpinnerModule,
    MatCardModule,
    MatCheckboxModule,
  ],
  exports: [
    MatProgressSpinnerModule,
    MatCardModule,
    MatCheckboxModule,
  ]
})
export class MaterialModule {}
