import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {ProjectService} from './project.service'
import * as ProjectActions from './store/project.actions'
import * as fromReducer from './store/project.reducer'
import { Store,select } from '@ngrx/store';
import {Observable} from 'rxjs'
@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  id: number = 0;
  project:any = {};
  state: Observable<fromReducer.State>;

  constructor(private service:ProjectService, private route: ActivatedRoute, private store: Store<fromReducer.State>) {
     this.state = this.store.select('project')
  }

  ngOnInit() {
    this.route.params.subscribe(routeParams=>{
      this.id = this.route.snapshot.params.id;
      this.store.dispatch(new ProjectActions.Fetch(this.id))
    })

    this.state.subscribe((data)=>{console.log(data)})
  }

}
