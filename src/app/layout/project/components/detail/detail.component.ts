import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  @Input() project;
  constructor() { }

  ngOnInit() {
  }

  getProjectOptions() {
    let options = '';

    if(this.project.options.execution_time) {
      options += 'Execution Time, '
    }

    if(this.project.options.memory) {
      options += 'Memory, '
    }

    if(this.project.options.cpu) {
      options += 'CPU, '
    }

    if(options) {
      options = options.slice(0,-2)
    }

    return options
  }

}
