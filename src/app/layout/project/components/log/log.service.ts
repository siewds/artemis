import {Injectable,Input} from '@angular/core';
import {HttpClient,HttpRequest,HttpEventType,HttpResponse,HttpParams,HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class LogService {

  @Input() id: number;
  constructor(private http:HttpClient) {

  }

  fetchMessages(id:number) {
     let url = 'http://localhost:3000/projects/' + id
     return this.http.get(url)
  }

}
