import { Component, OnInit,Input,OnDestroy,ViewChild ,ElementRef} from '@angular/core';
import {LogService} from './log.service'
@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit,OnDestroy {
  @Input() id: number;
  interval;
  messages : string[] = []
  @ViewChild('log') log :ElementRef;
  constructor(private service:LogService) { }

  ngOnInit() {
    console.log('ngOnInit')
    this.interval = setInterval(()=>{
      console.log('pushing')
      this.messages.push("This is changes ");
      this.scrollToBottom()
      // that.service.fetchMessages(that.id).subscribe((data)=>{this.messages = (data.messages ? data.messages : [])})
    },1000)

  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  scrollToBottom(): void {
    try {
        this.log.nativeElement.scrollToBottom = 0
    } catch(err) { }
   }



}
