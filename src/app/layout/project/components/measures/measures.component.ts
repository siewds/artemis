import { Component, OnInit, ViewChild, OnDestroy} from '@angular/core';
import * as shape from 'd3-shape';
import * as d3 from 'd3';
@Component({
  selector: 'app-measures',
  templateUrl: './measures.component.html',
  styleUrls: ['./measures.component.scss']
})
export class MeasuresComponent implements OnInit, OnDestroy {
  @ViewChild('lineChart') lineChart;
  interval;
    constructor() { }
    ngOnInit() {
      this.interval = setInterval(()=>{
        console.log('pushing')
        this.randomize()
        // that.service.fetchMessages(that.id).subscribe((data)=>{this.messages = (data.messages ? data.messages : [])})
      },3000)

    }

    ngOnDestroy() {
      clearInterval(this.interval);

    }

    public lineChartData:Array<any> = [
     {data: [65, 59, 80, 81, 56, 55, 40,65, 59, 80, 81, 56, 55, 40,11], label: 'Series A',fill: false,tension:0},
     {data: [28, 48, 40, 19, 86, 27, 90,28, 48, 40, 19, 86, 27, 90,11], label: 'Series B',fill: false,tension:0},
   ];
   public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
   public lineChartOptions:any = {
     responsive: true,
     tension:0,
     lineTension:0,
     bezierCurve: false

   };
   public lineChartColors:Array<any> = [
     { // grey
       backgroundColor: 'rgba(148,159,177,0.2)',
       borderColor: 'rgba(148,159,177,1)',
       pointBackgroundColor: 'rgba(148,159,177,1)',
       pointBorderColor: '#fff',
       pointHoverBackgroundColor: '#fff',
       pointHoverBorderColor: 'rgba(148,159,177,0.8)'
     },
     { // dark grey
       backgroundColor: 'rgba(77,83,96,0.2)',
       borderColor: 'rgba(77,83,96,1)',
       pointBackgroundColor: 'rgba(77,83,96,1)',
       pointBorderColor: '#fff',
       pointHoverBackgroundColor: '#fff',
       pointHoverBorderColor: 'rgba(77,83,96,1)'
     }
   ];
   public lineChartLegend:boolean = true;
   public lineChartType:string = 'line';



   public randomize():void {
     let _lineChartData:Array<any> = new Array(this.lineChartData.length);
     for (let i = 0; i < this.lineChartData.length; i++) {
       _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
       for (let j = 0; j < this.lineChartData[i].data.length; j++) {
         _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
       }
     }


     this.lineChartData = _lineChartData;
   }

   // events
   public chartClicked(e:any):void {
     console.log(e);
   }

   public chartHovered(e:any):void {
     console.log(e);
   }


}
