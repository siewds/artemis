import * as ProjectActions from './project.actions';

export interface State {
  loading: boolean;
  id : number;
  project : any;
}

const initialState = {
  loading: false,
  id : 0,
  project : {}
}

export function projectReducer(state=initialState,action: ProjectActions.Actions) {
  switch(action.type) {
    case ProjectActions.FETCH : {
       return {
         ...state,
         loading:true,
         id: action.payload
       }
    }

    case ProjectActions.FETCH_DONE : {
      return {
        ...state,
        loading:false,
        project : action.payload
      }
    }

    default: {
      return state;
    }
  }
}
