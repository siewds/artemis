import {Injectable} from '@angular/core';
import {Action,Store} from '@ngrx/store';

export const FETCH = 'Project Fetch';
export const FETCH_DONE = 'Project Upload Done'

export class Fetch implements Action {
  readonly type = FETCH;
  constructor(public payload: number ) {}
}

export class FetchDone implements Action {
  readonly type = FETCH_DONE;
  constructor(public payload: any) {}
}

export type Actions = Fetch | FetchDone;
