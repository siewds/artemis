import {Injectable} from '@angular/core';
import {Actions,Effect,ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs';
import {catchError,map,switchMap} from 'rxjs/operators';
import * as ProjectActions from './project.actions';
import {ProjectService} from '../project.service'

@Injectable(

)
export class ProjectEffects {
  constructor(private actions$: Actions,private service: ProjectService) {

  }

  @Effect()
  public fetchProject$: Observable<Action> = this.actions$.pipe(
    ofType(ProjectActions.FETCH),
    map((action:ProjectActions.Fetch)=>action.payload),
    switchMap((id)=>{
      return this.service.fetchProject(id).pipe(
        map(project => new ProjectActions.FetchDone(project))
      )
    })
  )

}
