import {Injectable} from '@angular/core';
import {HttpClient,HttpRequest,HttpEventType,HttpResponse,HttpParams,HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  constructor(private http:HttpClient) {

  }

  fetchProject(id:number) {
     let url = 'http://localhost:3000/projects/' + id
     return this.http.get(url)
  }

}
