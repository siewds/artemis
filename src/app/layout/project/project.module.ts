import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import { ProjectRoutingModule } from './project-routing.module';
import { ProjectComponent } from './project.component';
import { PageHeaderModule , UploadModule} from './../../shared';
import {MaterialModule} from './material.module';
import {DetailComponent, MeasuresComponent,LogComponent} from './components'
import {ProjectService} from './project.service'
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {ProjectEffects} from './store/project.effects'
import * as fromProject from './store/project.reducer'
import {LogService} from './components/log/log.service'
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
    imports: [
      NgxChartsModule,
      CommonModule, ProjectRoutingModule, Ng2Charts,PageHeaderModule,MaterialModule,FormsModule,ReactiveFormsModule,
      StoreModule.forRoot({project : fromProject.projectReducer}),
      EffectsModule.forRoot([ProjectEffects]),

    ],
    declarations: [ProjectComponent,DetailComponent,MeasuresComponent,LogComponent],
    providers: [ProjectService,LogService]

})
export class ProjectModule {}
