import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import { TableRoutingModule } from './table-routing.module';
import { TableComponent } from './table.component';
import { PageHeaderModule , UploadModule} from './../../shared';
import {MaterialModule} from './material.module';
import {TableService} from './table.service'

@NgModule({
    imports: [CommonModule, TableRoutingModule, PageHeaderModule,MaterialModule,FormsModule,ReactiveFormsModule],
    declarations: [TableComponent],
    providers: [TableService]

})
export class TableModule {}
