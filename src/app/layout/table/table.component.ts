import { Component, OnInit , ViewChild} from '@angular/core';
import {map,switchMap} from 'rxjs/operators'
import {TableService} from './table.service'
import {MatPaginator} from '@angular/material'
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  displayedColumns: string[] = ['name', 'execution_time', 'memory', 'cpu','link','created_at'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  loading = false;
  data:any = [];
  page:number = 1;
  totalResults:string = '0';
  pageSize:number = 5;
  constructor(private service: TableService,private router:Router) { }



  ngOnInit() {

     this.service.fetchProjects(this.page,this.pageSize).subscribe(data=>{this.totalResults = data.headers.get('x-total-count'); this.data = data.body})
     this.paginator.page.subscribe(
       (data)=>{
           this.service.fetchProjects(data.pageIndex + 1,data.pageSize).subscribe(data=>{this.totalResults = data.headers.get('x-total-count'); this.data = data.body})
       })

  }

}
