import {NgModule} from '@angular/core';

import {
  MatProgressSpinnerModule,
  MatTableModule,
  MatPaginatorModule
} from '@angular/material';

@NgModule({
  imports: [
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatTableModule
  ],
  exports: [
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatTableModule
  ]
})
export class MaterialModule {}
