
import { Injectable } from '@angular/core';
import {HttpClient,HttpRequest,HttpEventType,HttpResponse,HttpParams,HttpHeaders} from '@angular/common/http';
import {Http,Response} from '@angular/http';
import {Observable,Subject} from 'rxjs';
import {map,switchMap} from 'rxjs/operators'

@Injectable()
export class TableService {

  constructor(private http:HttpClient) {

  }

  public fetchProjects(page,pageSize)  {
    let url =  'http://localhost:3000/projects?_limit=' + pageSize + '&_sort=created_at&_order=desc'
    if(page>0) {
       url+= ('&_page=' + page)
    }

    return this.http.get(url, {observe: 'response'})
  }


}
