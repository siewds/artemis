import { Injectable } from '@angular/core';
import {HttpClient,HttpRequest,HttpEventType,HttpResponse,HttpParams,HttpHeaders} from '@angular/common/http';
import {Observable,Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable()
export class OptimizeService {

  constructor(private http:HttpClient,public db:AngularFirestore) {

  }

  public createProject(data) {
    data['created_at'] = new Date()
    let url = 'http://localhost:3000/projects'
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const requestOptions = {
      headers : headers
    };

    return this.http.post(url,data,requestOptions)
  }


}
