import {NgModule} from '@angular/core';

import {
  MatProgressSpinnerModule,
  MatCardModule,
  MatCheckboxModule,
  MatInputModule
} from '@angular/material';

@NgModule({
  imports: [
    MatProgressSpinnerModule,
    MatCardModule,
    MatCheckboxModule,
    MatInputModule
  ],
  exports: [
    MatProgressSpinnerModule,
    MatCardModule,
    MatCheckboxModule,
    MatInputModule
  ]
})
export class MaterialModule {}
