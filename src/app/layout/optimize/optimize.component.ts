import { Component, OnInit ,ViewChild} from '@angular/core';
import {FormGroup,FormControl,Validators} from '@angular/forms';
import {OptimizeService} from './optimize.service'
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-optimize',
  templateUrl: './optimize.component.html',
  styleUrls: ['./optimize.component.scss']
})
export class OptimizeComponent implements OnInit {
  @ViewChild('upload') upload;
  form:FormGroup;
  loading:boolean = false
  fileValid : boolean = false;
  linkValid: boolean = false;
  constructor(private service:OptimizeService,private router:Router,private toastr: ToastrService) { }

  ngOnInit() {
   this.form = new FormGroup({
     name : new FormControl('',Validators.required),
     options : new FormGroup({
       execution_time : new FormControl(),
        memory : new FormControl(),
        cpu : new FormControl(),
     },this.optionsValidator),
       link : new FormGroup({
       file: new FormControl(),
       repository : new FormControl()
     },this.linkValidator)
   })
  }

  linkValidator(group) : { [key: string]: boolean } | null {
    if(group.controls.repository.value || group.controls.file.value) {
      return null;
    }else {
      return {'linkValid' : false}
    }
  }

  optionsValidator(group): { [key: string]: boolean } | null {
    console.log(group)
    if(group.controls.cpu.value  || group.controls.execution_time.value || group.controls.memory.value) {
      return null;
    }else {
      return {'optionsValid' : false}
    }
  }

  submit() {
    if(this.form.valid) {
      if(this.upload.isValid()) {
        this.loading = true;
        this.upload.upload().subscribe(val=>{
          if(val) {
                this.service.createProject({...this.form.value,link:val}).subscribe(val=>{console.log('completed'); this.loading = false; this.toastr.success("Project Submitted");this.router.navigate([""]);})
          }else {
            this.loading = false;
          }
        })
      }else {
           this.service.createProject({...this.form.value,link:this.form.controls['link']['controls']['repository'].value}).subscribe(val=>{console.log('completed');    this.loading = false; this.toastr.success("Project Submitted"); this.router.navigate([""]);})
      }
    }
  }




  onChange(event) {
    console.log(event)
    console.log(this.form.controls['link']['controls']['file'])
    this.form.controls['link']['controls']['file'].markAsTouched()
    this.form.controls['link']['controls']['file'].patchValue(event)
    this.form.controls.link.updateValueAndValidity()
  }

}
