import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import { OptimizeRoutingModule } from './optimize-routing.module';
import { OptimizeComponent } from './optimize.component';
import { PageHeaderModule , UploadModule} from './../../shared';
import {MaterialModule} from './material.module';
import {OptimizeService} from './optimize.service'
import { ToastrModule } from 'ngx-toastr';

@NgModule({
    imports: [CommonModule, OptimizeRoutingModule, PageHeaderModule,MaterialModule,UploadModule,FormsModule,ReactiveFormsModule,],
    declarations: [OptimizeComponent],
    providers: [OptimizeService]

})
export class OptimizeModule {}
