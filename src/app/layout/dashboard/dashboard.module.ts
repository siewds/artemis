import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import {
    NotificationComponent,
    SocketMessageComponent,
} from './components';
import { StatModule } from '../../shared';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {DashboardEffects} from './store/dashboard.effects'
import * as fromDashboard from './store/dashboard.reducer'
import {DashboardService} from './dashboard.service'
@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
        DashboardRoutingModule,
        StatModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        HttpClientModule,
        StoreModule.forRoot(fromDashboard.dashboardReducer),
        EffectsModule.forRoot([DashboardEffects]),
    ],
    declarations: [
        DashboardComponent,
        NotificationComponent,
        SocketMessageComponent,
    ],
    providers: [DashboardService],

})
export class DashboardModule {}
