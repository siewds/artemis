import * as DashboardActions from './dashboard.actions';

export interface State {
  loading: boolean;
  uploadFile : File;
  result : File[];
}

const initialState = {
  loading: false,
  uploadFile : null,
  result : []
}

export function dashboardReducer(state=initialState,action: DashboardActions.Actions) {
  switch(action.type) {
    case DashboardActions.UPLOAD : {
       return {
         ...state,
         loading:true,
         uploadFile: action.payload
       }
    }

    case DashboardActions.UPLOAD_DONE : {
      return {
        ...state,
        loading:false,
        result : action.payload
      }
    }

    default: {
      return state;
    }
  }
}
