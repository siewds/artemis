import {Injectable} from '@angular/core';
import {Actions,Effect,ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs';
import {catchError,map,switchMap} from 'rxjs/operators';
import * as DashboardActions from './dashboard.actions';
import {DashboardService} from '../dashboard.service'

@Injectable(

)
export class DashboardEffects {
  constructor(private actions$: Actions,private service: DashboardService) {

  }

  @Effect()
  public uploadFile$: Observable<Action> = this.actions$.pipe(
    ofType(DashboardActions.UPLOAD),
    map((action:DashboardActions.Upload)=>action.payload),
    switchMap((file)=>{
      return this.service.upload(file).pipe(
        map(results => new DashboardActions.UploadDone(results))
      )
    })
  )

}
