import {Injectable} from '@angular/core';
import {Action,Store} from '@ngrx/store';

export const UPLOAD = 'Dashboard Upload';
export const UPLOAD_DONE = 'Dashboard Upload Done'

export class Upload implements Action {
  readonly type = UPLOAD;
  constructor(public payload: File ) {}
}

export class UploadDone implements Action {
  readonly type = UPLOAD_DONE;
  constructor(public payload: File[]) {}
}

export type Actions = Upload | UploadDone;
