import {Injectable} from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  constructor(private http:Http) {

  }

  upload(file:File) : Observable<File[]>{
    let formData = new FormData();
    formData.append("file", file);
    return this.http.post('https://google.co.uk',formData).pipe(
      map((response:Response)=>{
        return response.json() ? response.json().map(item=> {
          return new File([""], "filename");
        }) : []
      })
    )
  }
}
