// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase : {
   apiKey: 'AIzaSyCbtkTHXAu-KMCRVwwI7hXCfxLtYluFBU0',
   projectId: 'artemis-4f4b9',
   storageBucket: 'artemis-4f4b9.appspot.com',
   databaseURL : 'https://artemis-4f4b9.firebaseio.com/'
  },
};
